package cn.bootx.common.core.code.actable;

/**
 * 数据库类型
 * @author xxm
 * @date 2023/1/16
 */
public enum DatabaseType {
    MYSQL,
    ORACLE,
    SQLSERVER,
    POSTGRESQL;
}
