package cn.bootx.common.log;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
* 日志扫描
* @author xxm
* @date 2022/6/6
*/
@ComponentScan
@AutoConfiguration
public class LogAutoConfiguration {
}
