package cn.bootx.common.actable.constants;

public interface Constants {

	String NEW_TABLE_MAP					= "newTableMap";
	String MODIFY_TABLE_MAP					= "modifyTableMap";
	String ADD_TABLE_MAP					= "addTableMap";
	String REMOVE_TABLE_MAP					= "removeTableMap";
	String MODIFY_TABLE_PROPERTY_MAP 		= "modifyTablePropertyMap";
	String DROPKEY_TABLE_MAP				= "dropKeyTableMap";
	String DROPINDEXANDUNIQUE_TABLE_MAP		= "dropIndexAndUniqueTableMap";
	String ADDINDEX_TABLE_MAP				= "addIndexTableMap";
	String ADDUNIQUE_TABLE_MAP				= "addUniqueTableMap";

}
