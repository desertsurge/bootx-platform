package cn.bootx.baseapi.core.sql.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * SQL查询参数
 * @author xxm
 * @date 2023/3/9
 */
@Getter
@Setter
public class QuerySqlParam {
}
