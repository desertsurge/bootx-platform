package cn.bootx.baseapi.core.sql.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * SQL查询结果字段
 * @author xxm
 * @date 2023/3/9
 */
@Getter
@Setter
public class QuerySqlField {
}
